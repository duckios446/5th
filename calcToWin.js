function checkWin(gameField) {
  // Проверяем горизонтали и вертикали
  for (let i = 0; i < 3; i++) {
    if (gameField[i][0] === gameField[i][1] && gameField[i][1] === gameField[i][2]) {
      if (gameField[i][0] === "x") {
        return "Крестики победили";
      } else if (gameField[i][0] === "o") {
        return "Нолики победили";
      }
    }
    if (gameField[0][i] === gameField[1][i] && gameField[1][i] === gameField[2][i]) {
      if (gameField[0][i] === "x") {
        return "Крестики победили";
      } else if (gameField[0][i] === "o") {
        return "Нолики победили";
      }
    }
  }
  // Проверяем диагонали
  if (gameField[0][0] === gameField[1][1] && gameField[1][1] === gameField[2][2]) {
    if (gameField[0][0] === "x") {
      return "Крестики победили";
    } else if (gameField[0][0] === "o") {
      return "Нолики победили";
    }
  }
  if (gameField[0][2] === gameField[1][1] && gameField[1][1] === gameField[2][0]) {
    if (gameField[0][2] === "x") {
      return "Крестики победили";
    } else if (gameField[0][2] === "o") {
      return "Нолики победили";
    }
  }
  // Если никто не победил и есть пустые клетки, то игра продолжается
  for (let i = 0; i < 3; i++) {
    for (let j = 0; j < 3; j++) {
      if (gameField[i][j] === null) {
        return "Игра продолжается";
      }
    }
  }
  // Если никто не победил и нет пустых клеток, то игра закончилась вничью
  return "Ничья";
}
// Пример
const gameField = [
  ["x", "o", null],
  ["x", null, "o"],
  ["x", "o", "o"],
];

//document.getElementById('min').textContent = checkWin(gameField);
console.log(checkWin(gameField)); // Выведет "Крестики победили"
